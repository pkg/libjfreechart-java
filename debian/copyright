Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: JFreeChart
Source: https://sourceforge.net/projects/jfreechart/files/1.%20JFreeChart/
Files-Excluded:
 *.jar
 *.class
 lib/*

Files: *
Copyright: David Gilbert (david.gilbert@object-refinery.com) and others
           2000-2013, Object Refinery Limited and Contributors
License: LGPL-2.1+
Comment:
 Upstream Authors:
 .
 Thanks to the following developers who have contributed code to this
 class library:  Anthony Boulestreau, Jeremy Bowman, J. David
 Eisenberg, Paul English, Hans-Jurgen Greiner, Bill Kelemen, Achilleus
 Mantzios, Thomas Meier, Krzysztof Paz, Andrzej Porebski, Nabuo
 Tamemasa, Mark Watson and Hari.

Files: debian/*
Copyright: 2003,      Christian Bayle <bayle@debian.org>
           2005,      Arnaud Vandyck <avdyk@debian.org>
           2006,      Wolfgang Baer <WBaer@gmx.de>
           2006,      Matthias Klose <doko@debian.org>
           2007,      Michaek Koch <konqueror@gmx.de>
           2007,      Kumar Appaiah <akumar@ee.iitm.ac.in>
           2008,      Varun Hiremath <varun@debian.org>
           2009,      Vincent Fourmond <fourmond@debian.org>
           2010-2011, Damien Raude-Morvan <drazzib@debian.org>
           2012,      Jakub Adam <jakub.adam@ktknet.cz>
           2014-2016, Emmanuel Bourg <ebourg@apache.org>
           2015-2016, Markus Koschany <apo@debian.org>
License: LGPL-2.1+

Files: source/org/jfree/chart/demo/BarChartDemo1.java
       source/org/jfree/chart/fx/demo/BarChartFXDemo1.java
       source/org/jfree/chart/fx/demo/PieChartFXDemo1.java
       source/org/jfree/chart/fx/demo/TimeSeriesChartFXDemo1.java
Copyright: 2005-2014, Object Refinery Limited.
License: BSD-3-clause
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   - Neither the name of the Object Refinery Limited nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL OBJECT REFINERY LIMITED BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this package; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.
